﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.DTO;
using App.Domain.Model;
using App.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.API.Controllers
{
    [Route("api/assets")]
    [ApiController]
    public class AssetController : ControllerBase
    {
        private IAssetService assetService;
        public AssetController(IAssetService _assetService)
        {
            assetService = _assetService;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = await assetService.GetAllAsync();
            return Ok(data);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            //var data = await assetService.GetByIdAsync(id);
            var data = await assetService.GetAssetComponentByIdAsync(id);
            if (data == null) return Ok();
            return Ok(data);
        }
        [HttpPost]
        public async Task<IActionResult> Post(AssetDTO asset)
        {
            var data = await assetService.AddAssetComponent(asset);
            return Ok(data);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await assetService.DeleteAsync(id);
            return Ok(data);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(AssetDTO assetDTO, int id)
        {
            assetDTO.Asset.AssetID = id;
            var data = await assetService.UpdateAssetComponent(assetDTO);
            return Ok(data);
        }
    }
}