﻿using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.DTO
{
    public class AssetDTO
    {
        public Asset Asset { get; set; }
        public List<AssetComponent> Components { get; set; }
    }
}
