﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.DTO
{
    public class AssetComponentDTO
    {
        public int AssetComponentID { get; set; }
        public int AssetID { get; set; }
        public int ComponentID { get; set; }
        public int ComponentName { get; set; }
        public float Size { get; set; }
        public int TNom { get; set; }
        public int TMin { get; set; }
    }
}
