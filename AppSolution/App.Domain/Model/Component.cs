﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.Model
{
    public class Component
    {
        public int ComponentID { get; set; }
        public string ComponentName { get; set; }
    }
}
