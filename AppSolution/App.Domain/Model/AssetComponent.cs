﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.Model
{
    public class AssetComponent
    {
        public int AssetComponentID { get; set; }
        public int AssetID { get; set; }
        public string ComponentName { get; set; }
        public float Size { get; set; }
        public int TNom { get; set; }
        public int TMin { get; set; }
    }
}
