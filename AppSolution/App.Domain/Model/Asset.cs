﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain.Model
{
    public class Asset
    {
        public int AssetID { get; set; }
        public string Description { get; set; }
        public DateTime AnalysisDate { get; set; }
        public string Material { get; set; }
        public int YearBuilt { get; set; }
        public DateTime LastInspectionDate { get; set; }
        public DateTime NextInspectionDate { get; set; }
        public string DocumentStatus { get; set; }
        public string Comment { get; set; }

    }
}
