﻿using App.Domain.DTO;
using App.Domain.Model;
using App.Repository.Interfaces;
using App.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Service
{
    public class AssetService:IAssetService
    {
        private IAssetRepository assetRepository;
        private IAssetComponentRepository assetComponentRepository;
        public AssetService(IAssetRepository _assetRepository, IAssetComponentRepository _assetComponentRepository)
        {
            assetRepository = _assetRepository;
            assetComponentRepository = _assetComponentRepository;
        }
        public async Task<int> AddAsync(Asset asset)
        {
            var result = await assetRepository.AddAsync(asset);

            return result;
        }
        public async Task<int> DeleteAsync(int assetID)
        {
            var result = await assetRepository.DeleteAsync(assetID);
            await assetComponentRepository.DeleteAllByAssetIDAsync(assetID);
            return result;
        }
        public async Task<IReadOnlyList<Asset>> GetAllAsync()
        {
            var result = await assetRepository.GetAllAsync();
            return result;
        }
        public async Task<Asset> GetByIdAsync(int assetID)
        {
            var result = await assetRepository.GetByIdAsync(assetID);
            return result;
        }
        public async Task<int> UpdateAsync(Asset asset)
        {
            var result = await assetRepository.UpdateAsync(asset);
            return result;
        }
        public async Task<int> AddAssetComponent(AssetDTO assetDTO)
        {
            var assetID = await assetRepository.AddAsync(assetDTO.Asset);
            foreach(var item in assetDTO.Components)
            {
                item.AssetID = assetID;
            }
            var result = await assetComponentRepository.AddAssetComponents(assetDTO.Components);
            return result;
        }
        public async Task<AssetDTO> GetAssetComponentByIdAsync(int assetID)
        {
            AssetDTO assetDTO = new AssetDTO();
            var asset = await assetRepository.GetByIdAsync(assetID);
            assetDTO.Asset = asset;
            assetDTO.Components = await assetComponentRepository.GetAllByAssetIDAsync(assetID);
            return assetDTO;
        }
        public async Task<int> UpdateAssetComponent(AssetDTO assetDTO)
        {
            await assetRepository.UpdateAsync(assetDTO.Asset);
            foreach (var item in assetDTO.Components)
            {
                item.AssetID = assetDTO.Asset.AssetID;
            }
            await assetComponentRepository.DeleteAllByAssetIDAsync(assetDTO.Asset.AssetID);
            var result = await assetComponentRepository.AddAssetComponents(assetDTO.Components);
            return result;
        }
    }
}
