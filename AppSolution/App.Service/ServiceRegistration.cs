﻿using App.Repository.Interfaces;
using App.Service.Interfaces;
using App.Service;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Repository
{
    public static class ServiceRegistration
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddTransient<IAssetService, AssetService>();
        }
    }
}
