﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Service.Interfaces
{
    public interface IGenericService<T> where T : class
        {
            Task<T> GetByIdAsync(int id);
            Task<IReadOnlyList<T>> GetAllAsync();
            Task<int> AddAsync(T entity);
            Task<int> UpdateAsync(T entity);
            Task<int> DeleteAsync(int id);
        }
}
