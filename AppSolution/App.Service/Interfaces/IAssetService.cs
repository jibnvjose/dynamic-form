﻿using App.Domain.DTO;
using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Service.Interfaces
{
    public interface IAssetService:IGenericService<Asset>
    {
        Task<int> AddAssetComponent(AssetDTO assetDTO);
        Task<AssetDTO> GetAssetComponentByIdAsync(int assetID);
        Task<int> UpdateAssetComponent(AssetDTO assetDTO);
    }
}
