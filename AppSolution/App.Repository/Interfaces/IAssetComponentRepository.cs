﻿using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Interfaces
{
    public interface IAssetComponentRepository:IGenericRepository<AssetComponent>
    {
        Task<int> AddAssetComponents(List<AssetComponent> assetComponents);
        Task<List<AssetComponent>> GetAllByAssetIDAsync(int assetID);
        Task<int> DeleteAllByAssetIDAsync(int id);
    }
}
