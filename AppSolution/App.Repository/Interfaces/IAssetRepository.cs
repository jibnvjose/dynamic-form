﻿using App.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Repository.Interfaces
{
    public interface IAssetRepository:IGenericRepository<Asset>
    {
    }
}
