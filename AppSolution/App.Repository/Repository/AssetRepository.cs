﻿using App.Domain.Model;
using App.Repository.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository
{
    public class AssetRepository:IAssetRepository
    {
        private readonly IConfiguration configuration;
        public AssetRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<int> AddAsync(Asset entity)
        {
            //entity.AddedOn = DateTime.Now;
            var sql = "Insert into Asset (Description, AnalysisDate, Material,YearBuilt,LastInspectionDate,NextInspectionDate,DocumentStatus,Comment) VALUES (@Description, @AnalysisDate, @Material,@YearBuilt,@LastInspectionDate,@NextInspectionDate,@DocumentStatus,@Comment);SELECT CAST(SCOPE_IDENTITY() as int)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QuerySingleOrDefaultAsync<int>(sql, entity);
                return result;
            }
        }
        public async Task<int> DeleteAsync(int id)
        {
            var sql = "DELETE FROM Asset WHERE AssetID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, new { Id = id });
                return result;
            }
        }
        public async Task<IReadOnlyList<Asset>> GetAllAsync()
        {
            var sql = "SELECT AssetID, Description, AnalysisDate, Material,YearBuilt,LastInspectionDate,NextInspectionDate,DocumentStatus,Comment FROM Asset";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Asset>(sql);
                return result.ToList();
            }
        }
        public async Task<Asset> GetByIdAsync(int id)
        {
            var sql = "SELECT AssetID, Description, AnalysisDate, Material,YearBuilt,LastInspectionDate,NextInspectionDate,DocumentStatus,Comment FROM Asset WHERE AssetID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QuerySingleOrDefaultAsync<Asset>(sql, new { Id = id });
                return result;
            }
        }
        public async Task<int> UpdateAsync(Asset entity)
        {
            //entity.ModifiedOn = DateTime.Now;
            var sql = "UPDATE Asset SET Description = @Description, AnalysisDate = @AnalysisDate, Material = @Material,YearBuilt = @YearBuilt,LastInspectionDate = @LastInspectionDate,NextInspectionDate = @NextInspectionDate, DocumentStatus = @DocumentStatus, Comment = @Comment WHERE AssetID = @AssetID";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, entity);
                return result;
            }
        }
    }
}
