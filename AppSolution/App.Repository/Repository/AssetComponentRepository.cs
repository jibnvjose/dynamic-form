﻿using App.Domain.Model;
using App.Repository.Interfaces;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository
{
    public class AssetComponentRepository: IAssetComponentRepository
    {
        private readonly IConfiguration configuration;
        public AssetComponentRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<int> AddAsync(AssetComponent entity)
        {
            //entity.AddedOn = DateTime.Now;
            var sql = "Insert into AssetComponent (AssetID, Size, TNom,TMin,ComponentName) VALUES (AssetID, Size, TNom,TMin,ComponentName);)";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, entity);
                return result;
            }
        }
        public async Task<int> DeleteAsync(int id)
        {
            var sql = "DELETE FROM AssetComponent WHERE AssetComponentID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, new { Id = id });
                return result;
            }
        }
        public async Task<IReadOnlyList<AssetComponent>> GetAllAsync()
        {
            var sql = "SELECT AssetComponentID, AssetID, Size, TNom,TMin,ComponentName FROM AssetComponent";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<AssetComponent>(sql);
                return result.ToList();
            }
        }
        public async Task<AssetComponent> GetByIdAsync(int id)
        {
            var sql = "SELECT AssetComponentID, AssetID, Size, TNom,TMin,ComponentName FROM AssetComponent WHERE AssetComponentID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QuerySingleOrDefaultAsync<AssetComponent>(sql, new { Id = id });
                return result;
            }
        }
        public async Task<int> UpdateAsync(AssetComponent entity)
        {
            //entity.ModifiedOn = DateTime.Now;
            var sql = "UPDATE AssetComponent SET AssetID = @AssetID, Size = @Size, TNom = @TNom,TMin = @TMin,ComponentName = @ComponentName FROM AssetComponent WHERE AssetComponentID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, entity);
                return result;
            }
        }
        public async Task<int> AddAssetComponents(List<AssetComponent> assetComponents)
        {
            //entity.AddedOn = DateTime.Now;
            var sql = "Insert into AssetComponent (AssetID, Size, TNom,TMin,ComponentName) VALUES (@AssetID, @Size, @TNom,@TMin,@ComponentName);";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, assetComponents);
                return result;
            }
        }
        public async Task<List<AssetComponent>> GetAllByAssetIDAsync(int assetID)
        {
            var sql = "SELECT AssetComponentID, AssetID, Size, TNom,TMin,ComponentName FROM AssetComponent WHERE AssetID = @assetID";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<AssetComponent>(sql, new { assetID = assetID });
                return result.ToList();
            }
        }
        public async Task<int> UpdateAllAsync(List<AssetComponent> assetComponents)
        {
            //entity.ModifiedOn = DateTime.Now;
            var sql = "UPDATE AssetComponent SET AssetID = @AssetID, Size = @Size, TNom = @TNom,TMin = @TMin,ComponentName = @ComponentName FROM AssetComponent WHERE AssetComponentID = @AssetComponentID";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, assetComponents);
                return result;
            }
        }
        public async Task<int> DeleteAllByAssetIDAsync(int id)
        {
            var sql = "DELETE FROM AssetComponent WHERE AssetID = @Id";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, new { Id = id });
                return result;
            }
        }
    }
}
