﻿using App.Repository.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Repository
{
    public static class ServiceRegistration
    {
        public static void AddRepository(this IServiceCollection services)
        {
            services.AddTransient<IAssetRepository, AssetRepository>();
            services.AddTransient<IAssetComponentRepository, AssetComponentRepository>();
        }
    }
}
