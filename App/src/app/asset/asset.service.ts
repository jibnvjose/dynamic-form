import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Asset } from '../asset/asset';
import { AssetDto } from '../asset/asset-dto';

@Injectable({
  providedIn: 'root'
})
export class AssetService {
  private apiURL = "https://localhost:44325/api";  
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Asset[]> {
    return this.httpClient.get<Asset[]>(this.apiURL + '/assets/')
    .pipe(
      catchError(this.errorHandler)
    )
  }

  create(asset): Observable<Asset> {
    return this.httpClient.post<Asset>(this.apiURL + '/assets/', JSON.stringify(asset), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  find(assetID): Observable<AssetDto> {
    return this.httpClient.get<AssetDto>(this.apiURL + '/assets/' + assetID)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  update(assetID, asset): Observable<Asset> {
    return this.httpClient.put<Asset>(this.apiURL + '/assets/' + assetID, JSON.stringify(asset), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  delete(assetID){
    return this.httpClient.delete<Asset>(this.apiURL + '/assets/' + assetID, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
