import { Component, OnInit } from '@angular/core';
import { AssetService } from '../asset.service';
import { Asset } from '../asset';
import { AssetComponent } from '../asset-component';

import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  form: FormGroup;
  constructor(    
    private formBuilder: FormBuilder,
    public assetService: AssetService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({      
      Asset: this.formBuilder.group( {
        description: new FormControl('', [Validators.required]),
        analysisDate: new FormControl('', Validators.required),
        material: new FormControl('', Validators.required),
        yearBuilt: new FormControl(''),
        lastInspectionDate: new FormControl('', Validators.required),
        nextInspectionDate: new FormControl('', Validators.required),
        documentStatus: new FormControl(''),
        comment: new FormControl('')
      }),
      Components: this.formBuilder.array([this.createAssetComponentFormGroup()])
    });
  }
  get f(){
    return this.form.controls;
  }
  get description() {
    return this.form.get('Asset.description');
  }
  get analysisDate() {
    return this.form.get('Asset.analysisDate');
  }
  get material() {
    return this.form.get('Asset.material');
  }
  get lastInspectionDate() {
    return this.form.get('Asset.lastInspectionDate');
  }
  get nextInspectionDate() {
    return this.form.get('Asset.nextInspectionDate');
  }
  submit(){
    console.log(this.form.value);
    this.assetService.create(this.form.value).subscribe(res => {
         console.log('Asset created successfully!');
         this.router.navigateByUrl('asset/index');
    })
  }
  private createAssetComponentFormGroup(): FormGroup {
    return new FormGroup({
      'componentName': new FormControl('', Validators.required),
      'size': new FormControl(0),
      'tNom': new FormControl(0),
      'tMin': new FormControl(0)
    })
  }
  public addAssesComponentFormGroup() {
    const assetComponents = this.form.get('Components') as FormArray
    assetComponents.push(this.createAssetComponentFormGroup())
  }
  public removeOrClearAssesComponent(i: number) {
    const assetComponents = this.form.get('Components') as FormArray
    if (assetComponents.length > 1) {
      assetComponents.removeAt(i)
    } else {
      assetComponents.reset()
    }
  }
}
