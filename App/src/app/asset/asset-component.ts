export interface AssetComponent {
    assetComponentID: number;
    assetID: number;
    componentName: string;
    size: number;
    tNom: number;
    tMin: number;
}

