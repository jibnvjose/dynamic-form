import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'asset', redirectTo: 'asset/index', pathMatch: 'full'},
  { path: 'asset/index', component: IndexComponent },
  { path: 'asset/:assetID/view', component: ViewComponent },
  { path: 'asset/create', component: CreateComponent },
  { path: 'asset/:assetID/edit', component: EditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetRoutingModule { }
