import { AssetComponent } from './asset-component';
export interface Asset {
    assetID: number;
    description: string;
    analysisDate: number;
    material: string;
    yearBuilt: number;
    lastInspectionDate: string;
    nextInspectionDate: string;
    documentStatus: string;
    comment: string;
}
