import { Component, OnInit } from '@angular/core';
import { AssetService } from '../asset.service';
import { Asset } from '../asset'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  assets: Asset[] = [];
  constructor(public assetService:AssetService) { }

  ngOnInit(): void {
    this.assetService.getAll().subscribe((data:Asset[])=>{
      this.assets = data;
      console.log(this.assets);
    })
  }
  deleteSkillSet(assetID){
    this.assetService.delete(assetID).subscribe(res=>{
      this.assets = this.assets.filter(item=>item.assetID != assetID);
      console.log('Asset deleted successfully')
    })
  }
}
