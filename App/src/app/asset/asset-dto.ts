import { AssetComponent } from './asset-component';
import { Asset } from './asset';
export interface AssetDto {
    asset:Asset,
    components: AssetComponent[]
}

