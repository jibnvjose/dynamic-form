import { Component, OnInit } from '@angular/core';
import { AssetService } from '../asset.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetDto } from '../asset-dto';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray} from '@angular/forms';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: number;
  asset: AssetDto;
  form: FormGroup;
  constructor(    
    private formBuilder: FormBuilder,
    public assetService: AssetService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['assetID'];
    this.assetService.find(this.id).subscribe((data: AssetDto)=>{
      this.asset = data;
      var counter = 0;
      for(var item in this.asset.components ){
        if(++counter<this.asset.components.length)
        this.addAssesComponentFormGroup();
      }      
      (<FormArray>this.form.controls['Components']).patchValue(this.asset.components);
    });
    
    this.form = new FormGroup({      
      Asset: this.formBuilder.group( {
        description: new FormControl('', [Validators.required]),
        analysisDate: new FormControl('', Validators.required),
        material: new FormControl('', Validators.required),
        yearBuilt: new FormControl(''),
        lastInspectionDate: new FormControl('', Validators.required),
        nextInspectionDate: new FormControl('', Validators.required),
        documentStatus: new FormControl(''),
        comment: new FormControl('')
      }),
      Components: this.formBuilder.array([this.createAssetComponentFormGroup()])
    });
  }  
  get f(){
    return this.form.controls;
  }
  get description() {
    return this.form.get('Asset.description');
  }
  get analysisDate() {
    return this.form.get('Asset.analysisDate');
  }
  get material() {
    return this.form.get('Asset.material');
  }
  get lastInspectionDate() {
    return this.form.get('Asset.lastInspectionDate');
  }
  get nextInspectionDate() {
    return this.form.get('Asset.nextInspectionDate');
  }

  submit(){
    console.log(this.form.value);
    this.assetService.update(this.id, this.form.value).subscribe(res => {
         console.log('Asset updated successfully!');
         this.router.navigateByUrl('asset/index');
    })
  }
  private createAssetComponentFormGroup(): FormGroup {
    return new FormGroup({
      'componentName': new FormControl('', Validators.required),
      'size': new FormControl(0),
      'tNom': new FormControl(0),
      'tMin': new FormControl(0)
    })
  }
  public addAssesComponentFormGroup() {
    const assetComponents = this.form.get('Components') as FormArray
    assetComponents.push(this.createAssetComponentFormGroup())
  }
  public removeOrClearAssesComponent(i: number) {
    const assetComponents = this.form.get('Components') as FormArray
    if (assetComponents.length > 1) {
      assetComponents.removeAt(i)
    } else {
      assetComponents.reset()
    }
  }
}
