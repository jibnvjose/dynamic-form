USE [AppDB]
GO
/****** Object:  Table [dbo].[Asset]    Script Date: 26-01-2021 12:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asset](
	[AssetID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[AnalysisDate] [datetime] NULL,
	[Material] [varchar](50) NULL,
	[YearBuilt] [int] NULL,
	[LastInspectionDate] [datetime] NULL,
	[NextInspectionDate] [datetime] NULL,
	[DocumentStatus] [varchar](20) NULL,
	[Comment] [varchar](250) NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[AssetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssetComponent]    Script Date: 26-01-2021 12:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetComponent](
	[AssetComponentID] [int] IDENTITY(1,1) NOT NULL,
	[AssetID] [int] NULL,
	[Size] [float] NULL,
	[TNom] [int] NULL,
	[TMin] [int] NULL,
	[ComponentName] [varchar](50) NULL,
 CONSTRAINT [PK_AssetComponent] PRIMARY KEY CLUSTERED 
(
	[AssetComponentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
